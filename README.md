# Ansible Role: Notion Desktop

This role installs [Notion Desktop](https://notion.so/) client on any supported host.

## Requirements

None

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    notion_package_name: notion-app # notion-app or notion-enhanced

This role always installs the desired version. See [available Notion Desktop releases](https://github.com/notion-enhancer/notion-repackaged).

The name of the package installed in the system (if there need to be updated it).

    notion_package_name: notion-app # notion-app or notion-app-enhanced

## Dependencies

None.

## Installing

The role can be installed by running the following command:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-role-notion enmanuelmoreira.notion
```

Add the following line into your `ansible.cfg` file:

```bash
[defaults]
role_path = ../
```

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: enmanuelmoreira.notion

## License

MIT / BSD
